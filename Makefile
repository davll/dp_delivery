#
#
#

JFLAGS = -g
JC = javac

all: build
	$(JC) $(JFLAGS) -d build `find src -name *.java`
	@echo DONE

run:
	java -classpath build dp2011.group05.delivery.Main

doc: build
	javadoc -d build -attributes -operations -qualify -types -visibility -doclet org.umlgraph.doclet.UmlGraphDoc -docletpath UmlGraph-5.4.jar dp2011.group05.delivery `find src -name *.java`

build:
	mkdir build

clean:
	rm -rf build
	rm -f *.class

.SUFFIXES: .java .class
.PHONY: all run clean

