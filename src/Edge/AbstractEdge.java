package dp2011.group05.delivery;

public abstract class AbstractEdge implements Edge{
  
  //
  //protected abstract boolean acceptPackage(Package visitor);
  
  // fields
  private Location from;
  private Location to;
  private int time; // hrs
  private int cost; // dollars
  private int loadWeight; // kg
  
  // ctor
  public AbstractEdge(Location from, Location to)
  {
    this.from = from;
    this.to = to;
    this.time = 0;
    this.cost = 0;
    this.loadWeight = Integer.MAX_VALUE;
  }
  
  //
  @Override
  public final Location getFrom()
  {
    return from;
  }
  
  //
  public final Location getTo()
  {
    return to;
  }
  
  //
  @Override
  public final int getLoadWeight()
  {
    return loadWeight;
  }
  public final void setLoadWeight(int loadWeight)
  {
    this.loadWeight = loadWeight;
  }
  
  //
  @Override
  public final int getTime()
  {
    return time;
  }
  public final void setTime(int time)
  {
    if(time >= 0)
      this.time = time;
  }
  
  //
  @Override
  public final int getCost()
  {
    return cost;
  }
  public final void setCost(int cost)
  {
    if(cost >= 0)
      this.cost = cost;
  }
  
  //
  @Override
  public final boolean accept(Package visitor)
  {
    if(!from.accept(visitor) || !to.accept(visitor))
      return false;
    else if(getLoadWeight() < visitor.getWeight())
      return false;
    
    return visitor.visit(this);
  }
  
  //
  @Override
  public String toString()
  {
    return ("edge(" + from.getName() + ", " + to.getName() + "), "
            + "time = " + getTime() + ", cost = " + getCost());
  }
  
}
