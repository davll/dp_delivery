package dp2011.group05.delivery;

public abstract class AbstractEdgeDecorator implements Edge
{
  //
  protected abstract boolean acceptPackage(Package visitor);
  
  // fields
  protected Edge edge;
  
  // ctor
  public AbstractEdgeDecorator(Edge edge)
  {
    this.edge = edge;
  }
  
  //
  public final Edge getEdge()
  {
    return edge;
  }
  
  //
  
  @Override
  public final Location getTo()
  {
    return edge.getTo();
  }
  
  @Override
  public final Location getFrom()
  {
    return edge.getFrom();
  }
  
  //
  
  @Override
  public int getLoadWeight()
  {
    return edge.getLoadWeight();
  }
  
  @Override
  public int getTime()
  {
    return edge.getTime();
  }
  
  @Override
  public int getCost()
  {
    return edge.getCost();
  }
  
  //
  
  @Override
  public final boolean accept(Package visitor)
  {
    // strange!!!
    if(!getFrom().accept(visitor) || !getTo().accept(visitor))
      return false;
    else if(getLoadWeight() < visitor.getWeight())
      return false;
    
    return acceptPackage(visitor);
  }
  
  @Override
  public String toString()
  {
    return edge.toString();
  }
}