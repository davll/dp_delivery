package dp2011.group05.delivery;

public class Aero extends AbstractEdge
{
  public Aero(Location from, Location to)
  {
    super(from, to);
    setCost(2500);
    setTime(120);
    setLoadWeight(5000);
  }
  
  //@Override
  //public boolean acceptPackage(Package visitor)
  //{
  //  return visitor.visit(this);
  //}
  
  @Override
  public String toString()
  {
    return "aero." + super.toString();
  }
  
}
