package dp2011.group05.delivery;

public interface Edge{
  
  public Location getFrom();
  public Location getTo();
  
  public int getLoadWeight();
  public int getTime();
  public int getCost();
  
  public boolean accept(Package visitor);
  
  public String toString();
  
}
