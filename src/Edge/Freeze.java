package dp2011.group05.delivery;

public class Freeze extends AbstractEdgeDecorator
{
  public Freeze(Edge edge)
  {
    super(edge);
  }
  
  @Override
  public int getCost()
  {
    return 100 + super.edge.getCost();
  }
  
  @Override
  public int getTime()
  {
    return super.edge.getTime();
  }
  
  @Override
  public int getLoadWeight()
  {
    return super.edge.getLoadWeight();
  }
  
  @Override
  protected boolean acceptPackage(Package visitor)
  {
    return visitor.visit(this);
  }
  
  @Override
  public String toString()
  {
    return super.edge.toString() + ", freeze";
  }
}