package dp2011.group05.delivery;

public class Rail extends AbstractEdge{
  
  public Rail(Location from, Location to)
  {
    super(from, to);
    setCost(1000);
    setTime(20);
    setLoadWeight(20000);
  }
  
  //@Override
  //protected boolean acceptPackage(Package visitor)
  //{
  //  return visitor.visit(this);
  //}
  
  @Override
  public String toString()
  {
    return "rail." + super.toString();
  }
  
}
