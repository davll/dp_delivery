package dp2011.group05.delivery;

public class Road extends AbstractEdge{
  
  public Road(Location from, Location to)
  {
    super(from, to);
    setCost(200);
    setTime(60);
    setLoadWeight(800);
  }
  
  //@Override
  //protected boolean acceptPackage(Package visitor)
  //{
  //  return visitor.visit(this);
  //}
  
  @Override
  public String toString()
  {
    return "road." + super.toString();
  }
  
}
