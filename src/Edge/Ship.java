package dp2011.group05.delivery;

public class Ship extends AbstractEdge{
  
  public Ship(Location from, Location to)
  {
    super(from, to);
    setCost(1200);
    setTime(72 * 60);
    setLoadWeight(500000);
  }
  
  //@Override
  //protected boolean acceptPackage(Package visitor)
  //{
  //  return visitor.visit(this);
  //}
  
  @Override
  public String toString()
  {
    return "ship." + super.toString();
  }
  
}
