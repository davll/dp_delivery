package dp2011.group05.delivery;

import java.util.Comparator;

public class EdgeTimeComparator implements Comparator<Edge>{
  
  @Override
  public int compare(Edge o1, Edge o2)
  {
    if(o1.getTime() < o2.getTime())
      return -1;
    else if(o1.getTime() == o2.getTime())
      return 0;
    else
      return 1;
  }
  
  @Override
  public boolean equals(Object obj)
  {
    return super.equals(obj);
  }
  
}
