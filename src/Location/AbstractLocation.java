package dp2011.group05.delivery;

public abstract class AbstractLocation implements Location{
  
  // fields
  private String name;
  
  // ctor
  public AbstractLocation(String name)
  {
    this.name = name;
  }
  
  //
  @Override
  public final String getName()
  {
    return name;
  }
  
  //
  public final void setName(String name)
  {
    this.name = name;
  }
  
  //
  @Override
  public final boolean accept(Package visitor)
  {
    return visitor.visit(this);
  }
  
}
