package dp2011.group05.delivery;

public interface Location{
  
  public String getName();
  
  public boolean accept(Package visitor);
  
}
