package dp2011.group05.delivery;

import java.util.*;

public class Main{
  
  //
  public static void test100()
  {
    ShortestPath sp = new ShortestPath();
    Map<String, Location> locations = new HashMap<String, Location>();
    
    locations.put("A", new City("A"));
    locations.put("B", new City("B"));
    locations.put("C", new City("C"));
    locations.put("D", new City("D"));
    locations.put("E", new City("E"));
    locations.put("F", new City("F"));
    
    AbstractEdge e;
    e = new Road(locations.get("A"), locations.get("B"));
    e.setTime(10);
    sp.addEdge(e);
    
    e = new Ship(locations.get("A"), locations.get("B"));
    e.setTime(12);
    sp.addEdge(e);
    
    e = new Road(locations.get("B"), locations.get("C"));
    e.setTime(5);
    sp.addEdge(e);
    
    e = new Road(locations.get("C"), locations.get("C"));
    e.setTime(2);
    sp.addEdge(e);
    
    e = new Road(locations.get("C"), locations.get("B"));
    e.setTime(1);
    sp.addEdge(e);
    
    e = new Road(locations.get("E"), locations.get("B"));
    e.setTime(4);
    sp.addEdge(e);
    
    e = new Road(locations.get("E"), locations.get("C"));
    e.setTime(6);
    sp.addEdge(e);
    
    e = new Road(locations.get("B"), locations.get("D"));
    e.setTime(4);
    sp.addEdge(e);
    
    e = new Road(locations.get("D"), locations.get("F"));
    e.setTime(2);
    sp.addEdge(e);
    
    Package pak = new NormalPackage(10);
    Location from = locations.get("A"), to = locations.get("F");
    List<Edge> result = sp.compute(from, to, pak);
    
    for(Edge ed : result){
      System.out.println(ed.toString());
    }
  }
  
  //
  public static void test101()
  {
    ShortestPath sp = new ShortestPath();
    Map<String, Location> locations = new HashMap<String, Location>();
    
    locations.put("Hsinchu City", new City("Hsinchu City"));
    locations.put("Taipei City", new City("Taipei City"));
    locations.put("Taipei Main Station", 
                  new Station("Taipei Main Station"));
    locations.put("Hsinchu TRA Station", 
                  new Station("Hsinchu TRA Station"));
    locations.put("Hsinchu THSRC Station", 
                  new Station("Hsinchu THSRC Station"));
    
    Location from, to;
    AbstractEdge e;
    
    // Hsinchu City <-> Hsinchu TRA
    from = locations.get("Hsinchu City");
    to = locations.get("Hsinchu TRA Station");
    e = new Road(from, to);
    e.setTime(5);
    e.setCost(1);
    sp.addEdge(e);
    
    e = new Road(to, from);
    e.setTime(5);
    e.setCost(1);
    sp.addEdge(e);
    
    // Hsinchu TRA <-> Taipei Main Station
    from = locations.get("Hsinchu TRA Station");
    to = locations.get("Taipei Main Station");
    e = new Rail(from, to);
    e.setTime(90);
    e.setCost(100);
    sp.addEdge(e);
    
    e = new Rail(to, from);
    e.setTime(90);
    e.setCost(100);
    sp.addEdge(e);
    
    // Hsinchu City <-> Hsinchu THSRC
    from = locations.get("Hsinchu City");
    to = locations.get("Hsinchu THSRC Station");
    e = new Road(from, to);
    e.setTime(30);
    e.setCost(10);
    sp.addEdge(e);
    
    e = new Road(to, from);
    e.setTime(30);
    e.setCost(10);
    sp.addEdge(e);
    
    // Hsinchu THSRC <-> Taipei Main Station
    from = locations.get("Hsinchu THSRC Station");
    to = locations.get("Taipei Main Station");
    e = new Rail(from, to);
    e.setTime(30);
    e.setCost(290);
    sp.addEdge(e);
    
    e = new Rail(to, from);
    e.setTime(30);
    e.setCost(290);
    sp.addEdge(e);
    
    // Hsinchu City <-> Taipei City
    from = locations.get("Hsinchu City");
    to = locations.get("Taipei City");
    e = new Road(from, to);
    e.setTime(80);
    e.setCost(100);
    sp.addEdge(e);
    
    e = new Road(to, from);
    e.setTime(80);
    e.setCost(100);
    sp.addEdge(e);
    
    // Taipei City <-> Taipei Main Station
    from = locations.get("Taipei City");
    to = locations.get("Taipei Main Station");
    e = new Road(from, to);
    e.setTime(5);
    e.setCost(1);
    sp.addEdge(e);
    
    e = new Road(to, from);
    e.setTime(5);
    e.setCost(1);
    sp.addEdge(e);
    
    /////
    
    Package pak;
    List<Edge> result;
    
    from = locations.get("Hsinchu City");
    to = locations.get("Taipei City");
    
    pak = new NormalPackage(10);
    
    sp.setEdgeComparator(new EdgeTimeComparator());
    result = sp.compute(from, to, pak);
    System.out.println("result for shortest time:");
    for(Edge ed : result)
      System.out.println(ed.toString());
    
    System.out.println("");
    
    sp.setEdgeComparator(new EdgeCostComparator());
    result = sp.compute(from, to, pak);
    System.out.println("result for least cost:");
    for(Edge ed : result)
      System.out.println(ed.toString());
    
  }
  
  public static void test102()
  {
    ShortestPath sp = new ShortestPath();
    Map<String, Location> locations = new HashMap<String, Location>();
    
    locations.put("A", new City("A"));
    locations.put("B", new City("B"));
    locations.put("C", new City("C"));
    
    AbstractEdge e;
    e = new Road(locations.get("A"), locations.get("B"));
    e.setTime(10);
    sp.addEdge(e);
    
    e = new Ship(locations.get("A"), locations.get("B"));
    e.setTime(12);
    sp.addEdge(new Freeze(e));
    
    e = new Road(locations.get("B"), locations.get("C"));
    e.setTime(14);
    sp.addEdge(new Freeze(e));
    
    e = new Road(locations.get("B"), locations.get("C"));
    e.setTime(5);
    sp.addEdge(e);
    
    e = new Road(locations.get("C"), locations.get("C"));
    e.setTime(2);
    sp.addEdge(e);
    
    e = new Road(locations.get("C"), locations.get("B"));
    e.setTime(1);
    sp.addEdge(e);
    
    Location from, to;
    Package pak;
    List<Edge> result;
    
    from = locations.get("A");
    to = locations.get("C");
    
    System.out.println("result for NormalPackage: ");
    
    pak = new NormalPackage(10);
    result = sp.compute(from, to, pak);
    for(Edge ed : result)
      System.out.println(ed.toString());
    
    System.out.println("");
    
    System.out.println("result for Meat: ");
    
    pak = new Meat(10);
    result = sp.compute(from, to, pak);
    for(Edge ed : result)
      System.out.println(ed.toString());
    
  }
  
  ////////////////////////////////////////////////////////
  
  public static void test103()
  {
    
  }
  
  ////////////////////////////////////////////////////////
  
  // app main
  public static void main(String args[])
  {
    System.out.println("==== Test 100 ====");
    test100();
    System.out.println("==================");
    
    System.out.println("==== Test 101 ====");
    test101();
    System.out.println("==================");
    
    System.out.println("==== Test 102 ====");
    test102();
    System.out.println("==================");
    
  }
  
}
