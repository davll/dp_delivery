package dp2011.group05.delivery;

import java.lang.reflect.Method;

public abstract class AbstractPackage implements Package{
  
  // fields
  private int weight; // kg
  
  // ctor
  public AbstractPackage(int weight)
  {
    this.weight = weight;
  }
  
  //
  @Override
  public final int getWeight()
  {
    return weight;
  }
  
  // visit generic
  
  @Override
  public final <E extends Edge> boolean visit(E edge)
  {
    return this._visit(edge);
  }
  
  @Override
  public final <L extends Location> boolean visit(L location)
  {
    return this._visit(location);
  }
  
  //
  
  private final <T> boolean _visit(T obj)
  {
    // reflection to workaround
    Class<?> c = this.getClass();
    Class<?> oc = obj.getClass();
    
    Method visitMethod = null;
    boolean methodFound = false;
    
    while(!methodFound && !oc.equals(Object.class)){
      try{
        visitMethod = c.getMethod("_visit", oc);
        methodFound = true;
      }catch(NoSuchMethodException e){
        //e.printStackTrace();
        oc = oc.getSuperclass();
        methodFound = false;
      }
    }
    
    assert(methodFound == true);
    
    try{
      Boolean result = (Boolean) visitMethod.invoke(this, obj);
      return result.booleanValue();
    }catch(Exception e){
      return false;
    }
  }

}
