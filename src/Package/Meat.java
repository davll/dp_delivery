package dp2011.group05.delivery;

public class Meat extends AbstractPackage{
  
  // ctor
  public Meat(int weight)
  {
    super(weight);
  }
  
  // visit edge
  
  public boolean _visit(AbstractEdge edge)
  {
    return false;
  }
  
  public boolean _visit(AbstractEdgeDecorator de)
  {
    return false;
  }
  
  public boolean _visit(Freeze fe)
  {
    return true;
  }
  
  public boolean _visit(AbstractLocation loc)
  {
    return true;
  }
  
}
