package dp2011.group05.delivery;

public class NormalPackage extends AbstractPackage{
  
  // ctor
  public NormalPackage(int weight)
  {
    super(weight);
  }
  
  // visit edge
  
  public boolean _visit(AbstractEdge edge)
  {
    return true;
  }
  
  /*public boolean _visit(Aero flight)
  {
    return true;
  }
  
  public boolean _visit(Rail rail)
  {
    return true;
  }
  
  public boolean _visit(Road road)
  {
    return true;
  }
  
  public boolean _visit(Ship ship)
  {
    return true;
  }*/
  
  // visit edge decorator
  
  public boolean _visit(AbstractEdgeDecorator de)
  {
    return false;
  }
  
  /*
  public boolean _visit(Alive alive)
  {
    return true;
  }
  
  public boolean _visit(Fragile fragile)
  {
    return true;
  }
  
  public boolean _visit(Freeze freeze)
  {
    return true;
  }
  
  public boolean _visit(TimeLimit timelimit)
  {
    return true;
  }
  */
  
  // visit location
  
  public boolean _visit(AbstractLocation loc)
  {
    return true;
  }
  
  /*
  public boolean _visit(City city)
  {
    return true;
  }
  */
  
}
