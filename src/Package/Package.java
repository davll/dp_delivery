package dp2011.group05.delivery;

public interface Package{
  
  public int getWeight();
  
  public <E extends Edge> boolean visit(E edge);
  public <L extends Location> boolean visit(L location);
  
}
