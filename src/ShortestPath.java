package dp2011.group05.delivery;

import java.util.*;

public class ShortestPath{
  // Dijkstra
  // fields
  private Set<Location> locations;
  private Set<Edge> edges;
  private Comparator<? super Edge> edgeCompare;
  
  // ctor
  public ShortestPath()
  {
    this.locations = new HashSet<Location>();
    this.edges = new HashSet<Edge>();
    this.edgeCompare = new EdgeTimeComparator();
  }
  
  // add location
  public void addLocation(Location loc)
  {
    this.locations.add(loc);
  }
  
  // add edge
  public void addEdge(Edge edge)
  {
    Location from, to;
    from = edge.getFrom();
    to = edge.getTo();
    
    if(!locations.contains(from))
      this.addLocation(from);
    if(!locations.contains(to))
      this.addLocation(to);
    
    this.edges.add(edge);
  }
  
  // set edge comparator
  public void setEdgeComparator(Comparator<? super Edge> comp)
  {
    this.edgeCompare = comp;
  }
  
  ///////////////////////////////////////////////
  
  private class ConcatedEdge extends AbstractEdge{
    
    // field
    private Edge edge1;
    private Edge edge2;
    
    // ctor
    public ConcatedEdge(Edge edge1, Edge edge2)
    {
      super(edge1.getFrom(), edge2.getTo());
      
      if(!edge1.getTo().equals(edge2.getFrom()))
        throw new IllegalArgumentException("shit.");
      
      this.edge1 = edge1;
      this.edge2 = edge2;
      
      setLoadWeight(Math.min(edge1.getLoadWeight(), edge2.getLoadWeight()));
      setTime(this.edge1.getTime() + this.edge2.getTime());
      setCost(this.edge1.getCost() + this.edge2.getCost());
    }
    
    //
    protected boolean acceptPackage(Package visitor)
    {
      return this.edge1.accept(visitor) && this.edge2.accept(visitor);
    }
    
    //
    public String toString()
    {
      return (this.edge1.toString() + " => " + this.edge2.toString());
    }
    
  }
  
  ///////////////////////////////////////////////
  
  private class NullEdge implements Edge{
    
    // fields
    Location location;
    
    // ctor
    public NullEdge(Location location)
    {
      this.location = location;
    }
    
    //
    public final Location getFrom()
    {
      return this.location;
    }
    
    //
    public final Location getTo()
    {
      return this.location;
    }
    
    //
    public final int getLoadWeight()
    {
      return Integer.MAX_VALUE;
    }
    
    //
    public final int getTime()
    {
      return 0;
    }
    
    //
    public final int getCost()
    {
      return 0;
    }
    
    //
    public boolean accept(Package visitor)
    {
      return true;
    }
    
    //
    public String toString()
    {
      return "null_edge()";
    }
    
  }
  
  ///////////////////////////////////////////////
  
  // compute
  public List<Edge> compute(Location from, Location to, Package pak)
  {
    if(!locations.contains(from) || !locations.contains(to))
      return new ArrayList<Edge>();
    
    Map<Location, Set<Edge> > adjlist;
    adjlist = new HashMap<Location, Set<Edge> >();
    
    // Prepare Graph
    for(Location loc : this.locations){
      if(loc.accept(pak))
        adjlist.put(loc, new HashSet<Edge>() );
    }
    for(Edge ed : this.edges){
      if(ed.accept(pak))
        adjlist.get(ed.getFrom()).add(ed);
    }
    
    // Init Work Data
    SortedSet<Edge> relaxed = new TreeSet<Edge>(edgeCompare);
    Set<Location> used = new HashSet<Location>();
    Map<Location, Edge> data = new HashMap<Location, Edge>();
    Map<Location, Edge> prev = new HashMap<Location, Edge>();
    
    relaxed.add(new NullEdge(from));
    
    // Loop
    while(!relaxed.isEmpty()){
      Edge ud = relaxed.first();
      relaxed.remove(ud);
      
      Location u = ud.getTo();
      if(used.contains(u))
        continue;
      else
        used.add(u);
      
      for(Edge uv : adjlist.get(u) ){
        Location v = uv.getTo();
        Edge vd = data.get(v); // known edge: from->v
        Edge vd2 = new ConcatedEdge(ud, uv);
        // concate two edges: from->u and u->v
        
        if(vd == null || edgeCompare.compare(vd2, vd) < 0){ // vd2 < vd
          data.put(v, vd2);
          relaxed.add(vd2);
          prev.put(v, uv);
        }
      }
    }
    
    // Post Processing
    LinkedList<Edge> result = new LinkedList<Edge>();
    try{
      for(Location v = to; !v.equals(from);){
        Edge e = prev.get(v);
        result.addFirst(e);
        v = e.getFrom();
      }
    }catch(NullPointerException e){
      //e.printStackTrace();
      return new LinkedList<Edge>();
    }
    
    return result;
  }
  
}
